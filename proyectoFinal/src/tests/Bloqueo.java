package tests;

import java.util.ArrayList;

import api.Api;
import logica.Proceso;

public class Bloqueo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Api api = new Api();
		api.agregarProceso(new Proceso("a", 5, 1));
		api.agregarProceso(new Proceso("b", 5, 1));

		int i = 1 ;
		for (; i <= 3; i++) {
			System.out.println(i);
			api.avanzar();
			imprimirDatosGrafica(api);
		}
		api.bloquear();
		for (; i <= 5; i++) {
			System.out.println(i);
			api.avanzar();
			imprimirDatosGrafica(api);
		}
		api.desbloquear();
		
		for (; i <= 11; i++) {
			System.out.println(i);
			api.avanzar();
			imprimirDatosGrafica(api);
		}
		
	}

	public static void imprimirDatosGrafica(Api api) {
		ArrayList<Proceso> procesos = api.getDatosGrafica();
		Proceso ejecucion = api.getProcesoActualSeccionCritica();
		for (int i = 0; i < procesos.size(); i++) {
			System.out.println(procesos.get(i).getNombre() + " LLEGADA=" + procesos.get(i).getT_llegada() + " COMIENZO="
					+ procesos.get(i).getT_comienzo() + " FINAL=" + procesos.get(i).getT_final());
		}
		if (ejecucion != null) {
			System.out.println(ejecucion.getNombre() + " LLEGADA=" + ejecucion.getT_llegada() + " COMIENZO="
					+ ejecucion.getT_comienzo() + " FINAL=" + ejecucion.getT_final());
		}

	}

	public static void imprimirBloqueados(Api api) {
		String bloqueados = api.getColaBloqueados();
		System.out.println("BLOQUEADOS: " + bloqueados);
	}

}

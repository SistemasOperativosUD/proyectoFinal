package tests;

import api.Api;
import logica.Proceso;

public class EscalamientoProceso {

	public static void main(String[] args) {
		Api api = new Api();
		api.agregarProceso(new Proceso("a",10,1));
		api.agregarProceso(new Proceso("b",5,2));
		int i = 1;
		for( ; i<=6;i++) {
			System.out.println(i);
			api.avanzar();
			imprimirColas(api);
		}
		
	}
	public static void imprimirColas(Api api) {
		System.out.println("ROUND ROBBIN: "+api.getColaRoundRobbin());
		System.out.println("SFJ: "+api.getColaSFJ());
		System.out.println("PRIORIDAD: "+api.getColaPrioridad());
		System.out.println("EJECUCION: "+api.getEjecucion());
	}

}

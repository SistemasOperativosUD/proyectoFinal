package gui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AgregarProceso {
	JPanel contenedor;
	JLabel lblRafaga;
	JTextField txtRafaga;
	JLabel lblCola;
	JTextField txtCola;
	JButton btnAgregar;
	
	public AgregarProceso(ActionListener listener) {
		contenedor = new JPanel();
		lblRafaga = new JLabel("Rafaga: ");
		txtRafaga = new JTextField();
		txtRafaga.setColumns(3);
		lblCola = new JLabel("Cola: ");
		txtCola = new JTextField();
		txtCola.setColumns(3);
		btnAgregar = new JButton("AGREGAR");

		
		btnAgregar.addActionListener(listener);
		contenedor.add(lblRafaga);
		contenedor.add(txtRafaga);
		contenedor.add(lblCola);
		contenedor.add(txtCola);
		contenedor.add(btnAgregar);

	}

	public JPanel getContenedor() {
		return contenedor;
	}

	public void setContenedor(JPanel contenedor) {
		this.contenedor = contenedor;
	}

	public JLabel getLblRafaga() {
		return lblRafaga;
	}

	public void setLblRafaga(JLabel lblRafaga) {
		this.lblRafaga = lblRafaga;
	}

	public JTextField getTxtRafaga() {
		return txtRafaga;
	}

	public void setTxtRafaga(JTextField txtRafaga) {
		this.txtRafaga = txtRafaga;
	}

	public JLabel getLblCola() {
		return lblCola;
	}

	public void setLblCola(JLabel lblCola) {
		this.lblCola = lblCola;
	}

	public JTextField getTxtCola() {
		return txtCola;
	}

	public void setTxtCola(JTextField txtCola) {
		this.txtCola = txtCola;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}
}

package gui;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import api.Api;

public class Colas {
	JLabel finalizados;
	JLabel bloqueados;
	JLabel SFJ;
	JLabel ROUNDROBBIN;
	JLabel PRIORIDAD;
	JLabel SC;
	
	Api api;
	JPanel panel;
	public Colas(Api api) {
		this.api = api;
		SFJ = new JLabel("SFJ: Programa no iniciado");
		ROUNDROBBIN = new JLabel("ROUND ROBBIN: Programa no iniciado");
		PRIORIDAD = new JLabel("PRIORIDAD: Programa no iniciado");
		finalizados = new JLabel("Finalizados: Programa no iniciado");
		bloqueados = new JLabel ("Bloqueados: Programa no iniciado");
		SC = new JLabel ("SC: Programa no iniciado");
		

		panel= new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(new LineBorder(new Color(244,70,17)),"Colas"));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(ROUNDROBBIN);
		panel.add(SFJ);
		panel.add(PRIORIDAD);
		panel.add(finalizados);
		panel.add(bloqueados);
		panel.add(SC);

	}
	
	public JPanel getPanel() {
		return panel;
	}
	public void actualizarColas() {
		SFJ.setText("SFJ: "+api.getColaSFJ());
		ROUNDROBBIN.setText("Round robbin: "+api.getColaRoundRobbin());
		PRIORIDAD.setText("Prioridad: "+api.getColaPrioridad());
		
		finalizados.setText("Finalizados: "+api.getColaFinalizados());
		bloqueados.setText("Bloqueados: "+api.getColaBloqueados());
		
		SC.setText("EN EJECUCION : "+api.getEjecucion());

		panel.setBorder(BorderFactory.createTitledBorder(new LineBorder(new Color(57,255,20),3),"Colas"));

	}
}

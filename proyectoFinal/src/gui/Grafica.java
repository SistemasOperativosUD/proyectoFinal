package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import api.Api;
import logica.Proceso;

public class Grafica extends JFrame implements ActionListener {

	/* Logica */
	Api api = new Api();
	/* Elementos generales */
	Titulo titulo = new Titulo();
	Rejilla rejilla = new Rejilla(api);
	Tabla tabla = new Tabla(api);
	Botones botones = new Botones(this);
	TiempoActual tiempoActual = new TiempoActual(api);
	Colas colas = new Colas(api);
	AgregarProceso agregarProceso = new AgregarProceso(this);

	public Grafica() {
		this.setLayout(new GridBagLayout());
		this.setSize(1200, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(this);
		this.cargarElementos();
		this.setVisible(true);
	}

	public void cargarElementos() {
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5, 5, 5, 5);

		c.gridx = 0; // El área de texto empieza en la columna cero.
		c.gridy = 0; // El área de texto empieza en la fila cero
		c.gridwidth = 7; // El área de texto ocupa dos columnas.
		c.gridheight = 1; // El área de texto ocupa 2 filas.
		//this.add(titulo.getPanel(), c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 3;
		c.gridwidth = 7;
		this.add(rejilla.getPanel(), c);
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 7;
		this.add(colas.getPanel(), c);
		/*
		 * c.gridx = 0; c.gridy = 5; c.gridheight = 1; c.gridwidth = 7;
		 * this.add(tiempoActual.getPanel(),c);
		 */
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 3;
		c.gridwidth = 7;
		this.add(tabla.getPanel(), c);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 9;
		c.gridheight = 1;
		c.gridwidth = 7;
		this.add(botones.getPanel(), c);
		c.gridx = 0;
		c.gridy = 10;
		c.gridheight = 1;
		c.gridwidth = 7;
		this.add(agregarProceso.getContenedor(), c);

	}

	public void actualizarGrafica() {
		rejilla.actualizarDimensiones();
		tabla.repintarTabla();
		colas.actualizarColas();

		// tiempoActual.actualizarTiempoActual();
		this.getContentPane().validate();
		this.getContentPane().repaint();

	}

	public void nuevoProceso(int rafaga, int prioridad) {
		if (prioridad == 2) {
			Proceso proceso = new Proceso(this.getRandomNombre(),rafaga,prioridad);
			proceso.setPrioridadInterna(this.getRandomPrioridadInterna());
			api.agregarProceso(proceso);
		}else {
			Proceso agregar = new Proceso(this.getRandomNombre(), rafaga, prioridad);
			api.agregarProceso(agregar);
		}
		
		actualizarGrafica();

	}

	public void nuevoProceso() {
		int prioridad = this.getRandomPrioridad();
		if (prioridad == 2) {
			Proceso agregar = new Proceso(this.getRandomNombre(), this.getRandomRafaga(), prioridad);
			agregar.setPrioridadInterna(this.getRandomPrioridadInterna());
			api.agregarProceso(agregar);
		}else {
			Proceso agregar = new Proceso(this.getRandomNombre(), this.getRandomRafaga(), prioridad);
			api.agregarProceso(agregar);
		}
		actualizarGrafica();

	}

	public void siguienteIteracion() {
		api.avanzar();
		ArrayList<Proceso> procesos = api.getDatosGrafica();
		Proceso ejecucion = api.getProcesoActualSeccionCritica();
		for (int i = 0; i < procesos.size(); i++) {
			System.out.println(procesos.get(i).getNombre() + " LLEGADA=" + procesos.get(i).getT_llegada() + " COMIENZO="
					+ procesos.get(i).getT_comienzo() + " FINAL=" + procesos.get(i).getT_final());
		}
		if (ejecucion != null) {
			System.out.println(ejecucion.getNombre() + " LLEGADA=" + ejecucion.getT_llegada() + " COMIENZO="
					+ ejecucion.getT_comienzo() + " FINAL=" + ejecucion.getT_final());
		}
		actualizarGrafica();
	}

	public void bloquear() {
		api.bloquear();
	}

	private String getRandomNombre() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 2) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	private int getRandomRafaga() {
		Random rand = new Random(); // instance of random class
		int upperbound = 5;
		// generate random values from 0-7
		int int_random = rand.nextInt(upperbound);
		return int_random + 1+1;
	}

	private int getRandomPrioridad() {
		Random rand = new Random(); // instance of random class
		int upperbound = 3;
		// generate random values from 0-24
		int int_random = rand.nextInt(upperbound);
		return int_random;
	}
	private int getRandomPrioridadInterna() {
		Random rand = new Random(); // instance of random class
		int upperbound = 6;
		// generate random values from 0-24
		int int_random = rand.nextInt(upperbound);
		return int_random;
	}

	private void desbloquear() {
		api.desbloquear();
		actualizarGrafica();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == botones.getBtnAgregar()) {
			this.nuevoProceso();
		} else if (e.getSource() == botones.getBtnSiguiente()) {
			this.siguienteIteracion();
		} else if (e.getSource() == botones.getBtnBloquear()) {
			this.bloquear();
			JOptionPane.showMessageDialog(null, "El siguiente proceso sera bloqueado");
		} else if (e.getSource() == botones.getBtnDesbloquear()) {
			this.desbloquear();
		} else if(e.getSource() == agregarProceso.getBtnAgregar()) {
			this.nuevoProceso(Integer.parseInt(agregarProceso.getTxtRafaga().getText()),
					Integer.parseInt(agregarProceso.getTxtCola().getText()));
		}

	}

}

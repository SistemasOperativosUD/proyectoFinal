package gui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import api.Api;
import logica.Proceso;

public class Tabla {
	JPanel panel;
	Api api;
	JTable tabla;
	JScrollPane scroll;
	String[] columnas = {"PROCESO","PRIORIDAD","TIEMPO LLEGADA","RAFAGA","TIEMPO COMIENZO","TIEMPO FINAL","TIEMPO RETORNO","TIEMPO ESPERA"};
	DefaultTableModel modelo;
	
	public Tabla(Api api) {
		this.api = api;
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		String[][] datos = {{},{},{}};
		tabla= new JTable(new DefaultTableModel(datos, columnas));
		scroll=new JScrollPane(tabla);
		panel.add(scroll,BorderLayout.CENTER);
	}
	public void repintarTabla() {
		ArrayList<Proceso> datos = api.getDatosTabla();
		String[][] salida = new String[datos.size()][8];
		for(int i = 0 ; i<datos.size();i++) {
			salida[i][0]=datos.get(i).getNombre();
			salida[i][1]=String.valueOf(datos.get(i).getPrioridadGeneral());
			salida[i][2]=String.valueOf(datos.get(i).getT_llegada());
			salida[i][3]=String.valueOf(datos.get(i).getT_rafaga());
			salida[i][4]=String.valueOf(datos.get(i).getT_comienzo());
			salida[i][5]=String.valueOf(datos.get(i).getT_final());
			salida[i][6]=String.valueOf(datos.get(i).getT_retorno());
			salida[i][7]=String.valueOf(datos.get(i).getT_espera());
		}
		tabla.setModel(new DefaultTableModel(salida, columnas));

	}
	public JPanel getPanel() {
		return panel;
	}
}

package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import api.Api;
import logica.Proceso;

public class Rejilla {


    final int TIEMPO=50;
    int numProcesos;
    
    
    JPanel panel;    
    JLabel gridBtn[][];
    
    Api api;

    /**
     * Constructor de la rejilla, a partir del so, obtiene el numero de procesos
     * y el tiempo de simulacion
     * @param api 
     *
     * @param so El sistema operativo
     */
    public Rejilla(Api api) {
    	this.api = api;
        panel = new JPanel();

        numProcesos = api.getCantidadProcesos();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        actualizarDimensiones(4);

    }

    /**
     * Se actualizan las dimensiones de la rejilla, luego se llena de los datos
     */
    public void actualizarDimensiones() {
        panel.removeAll();
        numProcesos = api.getCantidadProcesos();

        Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
        gridBtn = new JLabel[numProcesos + 1][TIEMPO];

        for (int i = 0; i < numProcesos; i++) {

            JPanel aux = new JPanel();
            aux.setLayout(new GridLayout(1, TIEMPO));
            for (int j = 0; j < TIEMPO; j++) {

                gridBtn[i][j] = new JLabel(" ");
                gridBtn[i][j].setBackground(Color.WHITE);
                gridBtn[i][j].setBorder(border);
                gridBtn[i][j].setOpaque(true);
                aux.add(gridBtn[i][j]);
            }
            panel.add(aux);
        }

        actualizarDatos();

        panel.setBorder(BorderFactory.createTitledBorder(new LineBorder(new Color(0, 0, 0)), "Simulacion"));

    }

    /**
     * Se actualizan las dimensiones de la rejilla, luego se llena de los datos
     */
    public void actualizarDimensiones(int numProcesos) {
        panel.removeAll();

        Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
        gridBtn = new JLabel[numProcesos + 1][TIEMPO];

        for (int i = 0; i < numProcesos; i++) {

            JPanel aux = new JPanel();
            aux.setLayout(new GridLayout(1, TIEMPO));
            for (int j = 0; j < TIEMPO; j++) {

                gridBtn[i][j] = new JLabel(" ");
                gridBtn[i][j].setBackground(Color.WHITE);
                gridBtn[i][j].setBorder(border);
                gridBtn[i][j].setOpaque(true);
                aux.add(gridBtn[i][j]);
            }
            panel.add(aux);
        }
        actualizarDatos();
        panel.setBorder(BorderFactory.createTitledBorder(new LineBorder(new Color(0, 0, 0)), "Representacion"));

    }

    /**
     * Se obtienen los datos y se representan
     */
    public void actualizarDatos() {
        ArrayList<Proceso> datos = null;
        datos = api.getDatosGrafica();
        
     

        //Cantidad de procesos
        for (int i = 0 ; i<datos.size() && i<gridBtn.length ; i++) {
        	//Nombre del proceso
        	gridBtn[i][0].setText(datos.get(i).getNombre());

        	//Se pinta rango de inicio y final de ejecucion
        	int inicio = datos.get(i).getT_comienzo();
        	int fin = datos.get(i).getT_final()-1;
        	for(int j = inicio ; j<=fin && j<gridBtn[0].length ; j++) {
        		gridBtn[i][j].setBackground(Color.yellow);
        		gridBtn[i][j].setText(String.valueOf(j));
        		
        	}
        	//Se pinta rango de inicio y final de espera
        	inicio = datos.get(i).getT_llegada();
        	fin = datos.get(i).getT_comienzo()-1;
        	for(int j = inicio ; j<=fin ; j++) {
        		gridBtn[i][j].setBackground(Color.cyan);
        	}	
        	
        }
      //Se mira si hay uno en ejecucion  se grafica
  	  Proceso ejecucion = api.getProcesoActualSeccionCritica();
        if (ejecucion !=null) {
        	//Nombre del proceso
        	gridBtn[datos.size()][0].setText(ejecucion.getNombre());

        	//Se pinta rango de inicio y final de ejecucion
        	int inicio = ejecucion.getT_comienzo();
        	int fin = ejecucion.getT_final()-1;
        	for(int j = inicio ; j<=fin && j<gridBtn[0].length ; j++) {
        		gridBtn[datos.size()][j].setBackground(Color.yellow);
        		gridBtn[datos.size()][j].setText(String.valueOf(j));
        		
        	}
        	//Se pinta rango de inicio y final de espera
        	inicio = ejecucion.getT_llegada();
        	fin = ejecucion.getT_comienzo()-1;
        	for(int j = inicio ; j<=fin ; j++) {
        		gridBtn[datos.size()][j].setBackground(Color.cyan);
        	}	
        }
        	
    }

    /**
     * Se limpia la rejilla y se ponen todos los espacios en blanco
     */
    public void limpiarRejilla() {
        for (int i = 0; i < numProcesos; i++) {
            for (int j = 0; j < TIEMPO; j++) {
                gridBtn[i][j].setBackground(Color.WHITE);
            }
        }
    }

    /**
     * ********************************************
     */
    /**
     * ********************GETTERS*****************
     */
    /**
     * ********************************************
     */
    public JPanel getPanel() {
        return panel;
    }

    public int getNumeroProcesos() {
        return numProcesos;
    }

    public int getTiempo() {
        return TIEMPO;
    }
}

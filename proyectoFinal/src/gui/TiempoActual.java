package gui;

import javax.swing.JLabel;
import javax.swing.JPanel;

import api.Api;

public class TiempoActual {
	JLabel tiempoActual;
	
	JPanel panel;
	Api api;
	public TiempoActual(Api api) {
		this.api = api;
		panel = new JPanel();
		tiempoActual = new JLabel("Tiempo sin inicializar");
		panel.add(tiempoActual);
	}
	public void actualizarTiempoActual() {
		int tiempo = api.getTiempoActual();
		tiempoActual.setText("Tiempo actual: "+String.valueOf(tiempo));
	}
	public JPanel getPanel() {
		return panel;
		
	}
}

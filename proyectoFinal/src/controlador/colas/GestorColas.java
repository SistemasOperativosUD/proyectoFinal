package controlador.colas;

import java.util.ArrayList;

import logica.Proceso;
import logica.colas.*;

public class GestorColas {
	// Colas a emplear, static pues todas las instancias deben ser una sola
	private Cola[] colas = { new RoundRobbin(), new RafagaCorta(), new Prioridad() };
	private Cola bloqueados = new RafagaCorta();
	private ArrayList<Proceso> terminados = new ArrayList<Proceso>();

	
	/********************************************************/
	/********************************************************/
	/********************************************************/
	/*****************GESTION DE COLAS **********************/
	/********************************************************/
	/********************************************************/
	/********************************************************/

	
	
	/**
	 * Insertar un proceso el cual dependiendo su prioridad se insertara en una u
	 * otra cola
	 *
	 * @param El proceso a ingresar
	 */
	public void agregarProceso(Proceso proceso) {
		// Dependiendo de la prioridad que tenga el proceso, este se agrega
		if (proceso.getBloqueado() == false) {
			switch (proceso.getPrioridadGeneral()) {
			case 0:
				colas[0].agregar(proceso);
				break;
			case 1:
				colas[1].agregar(proceso);
				break;

			case 2:
				colas[2].agregar(proceso);
				break;
			}
		} else {
			bloqueados.agregar(proceso);
		}
	}

	public void insertarProcesoTerminado(Proceso proceso) {
		// Dependiendo de la prioridad que tenga el proceso, este se agrega
		terminados.add(proceso);

	}

	/**
	 * Se recorren las colas en orden preguntando si alguna tiene algun proceso en
	 * espera si lo tiene pues este sera el proceso a enviar a la seccion critica
	 * proceso. En caso de que no halla ningun proceso se retornada null
	 *
	 * @return El siguiente proceso a manejar
	 */
	public Proceso getSiguienteProceso() {
		if (colas[0].tieneProceso()) {
			return colas[0].desencolar();
		} else if (colas[1].tieneProceso()) {
			return colas[1].desencolar();
		} else if (colas[2].tieneProceso()) {
			return colas[2].desencolar();
		}
		return null;
	}

	public Proceso getSiguienteProcesoBloqueado() {
		if (bloqueados.tieneProceso()) {
			Proceso retornado = bloqueados.desencolar();
			retornado.setBloqueado(false);
			return retornado;
		}
		return null;
	}

	public Cola[] getColas() {
		return colas;
	}

	public void desbloquear() {
		Proceso desbloqueado = this.getSiguienteProcesoBloqueado();
		this.agregarProceso(desbloqueado);
	}

	public ArrayList<Proceso> getColaTerminados() {
		// TODO Auto-generated method stub
		return terminados;
	}

	public Cola getColaBloqueados() {
		return bloqueados;
	}

	public int getCantidadProcesosColasProtocolos() {
		int numeroProcesos = 0;

		Cola[] colas = this.getColas();
		// Se suman los procesos de las colas
		for (int i = 0; i < colas.length; i++) {
			numeroProcesos += colas[i].getTamano();
		}

		return numeroProcesos;
	}

	public void esNecesarioEscalamiento(int tiempoActual) {
		for(int i = 0 ; i<colas.length ; i++) {
			Proceso[] procesosCola = colas[i].getProcesos();
			for(int j = 0 ; j<procesosCola.length ; j++) {
				if(procesosCola[j].getT_comienzo() == -1) {
					if(tiempoActual - procesosCola[j].getT_llegada() >=9) {
						Proceso escalar = colas[i].desencolar(procesosCola[j]);
						realizarEscalamientoProceso(escalar);
					}
				}
			}
		}
	}

	public void realizarEscalamientoProceso(Proceso proceso) {
		if (proceso.getPrioridadGeneral() > 0) {
			proceso.setPrioridadGeneral(proceso.getPrioridadGeneral() - 1);
		}
		this.agregarProceso(proceso);

	}

	/*****************************************************/
	/*****************************************************/
	/*****************************************************/
	/************** COLAS EN STRING ************************/
	/*****************************************************/
	/*****************************************************/
	/*****************************************************/
	public String getColaBloqueadosFormateado() {
		Proceso[] bloqueados = this.getColaBloqueados().getProcesos();
		String salida = "";
		for (int i = 0; i < bloqueados.length; i++) {
			salida += bloqueados[i].getNombre() + "[R:" + bloqueados[i].getT_rafaga() + "]" + ", ";
		}
		return salida;

	}

	public String getColaRoundRobbinFormateado() {
		Proceso[] sjf = this.getColas()[0].getProcesos();
		String salida = "";
		for (int i = 0; i < sjf.length; i++) {
			salida += sjf[i].getNombre() + "[R:" + sjf[i].getT_rafaga() + "]" + ", ";
		}
		return salida;
	}

	public String getColaSFJFormateado() {
		Proceso[] sjf = this.getColas()[1].getProcesos();
		String salida = "";
		for (int i = 0; i < sjf.length; i++) {
			salida += sjf[i].getNombre() + "[R:" + sjf[i].getT_rafaga() + "]" + ", ";
		}
		return salida;
	}

	public String getColaPrioridadFormateado() {
		Proceso[] sjf = this.getColas()[2].getProcesos();
		String salida = "";
		for (int i = 0; i < sjf.length; i++) {
			salida += sjf[i].getNombre() + "[R:" + sjf[i].getT_rafaga() + "]" + "[P:" + sjf[i].getPrioridadInterna()
					+ "]" + ", ";
		}
		return salida;
	}

	public String getColaFinalizadosFormateado() {
		ArrayList<Proceso> terminados = this.getColaTerminados();
		String salida = "";
		for (int i = 0; i < terminados.size(); i++) {
			salida += terminados.get(i).getNombre() + "[R:" + terminados.get(i).getT_rafaga() + "]" + "[P:"
					+ terminados.get(i).getPrioridadGeneral() + "]" + ", ";
		}
		return salida;

	}



}

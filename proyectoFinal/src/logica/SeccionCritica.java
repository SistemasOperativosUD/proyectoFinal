package logica;

public class SeccionCritica {
	int duracionProceso;
	Proceso ejecucion;
	boolean bloquearSiguiente;
	int tiempoMaximoEjecucion = -1;
	int tiempoActual;
	public SeccionCritica() {
		ejecucion = null;
		duracionProceso = 0;
		bloquearSiguiente = false;
		tiempoActual = 1;
	}

	public boolean estaVacia() {
		if (ejecucion == null) {
			return true;
		}
		return false;
	}

	public boolean insertarProceso(Proceso siguienteProceso) {
		if(siguienteProceso != null) {
			if (this.estaVacia()) {
				ejecucion = siguienteProceso;
				ejecucion.setT_comienzo(tiempoActual);
				duracionProceso = 0;
				if (ejecucion.getInfoCola().getAlgoritmo() == "ROUNDROBBIN") {
					tiempoMaximoEjecucion = ejecucion.getInfoCola().getQuantum();
				}
				return true;
			}else {
				System.out.print("Se intento ingresar un proceso pero aun tenemos uno sc");
			}
		}else {
			System.out.println("Se intento ingresar un proceso nulo");
		}
		
		
		return false;

	}

	public Proceso[] avanzarUnPaso() {
		//Siempre y cuando no se deba bloquear nada
		if(ejecucion != null) {
			if (bloquearSiguiente == false) {
				tiempoActual++;
				duracionProceso++;
				//Dependiendo del algortimo usado se va a expulsar, ejecutar todo...
				switch (ejecucion.getInfoCola().getAlgoritmo()) {
				case "SFJ":
					//Se ejecuta todo el proceso
					if (duracionProceso < ejecucion.getT_rafaga()) {
						ejecucion.setT_final(tiempoActual);
						Proceso[] salida = {null,null};
						return salida;
					} else if (duracionProceso == ejecucion.getT_rafaga()) {
						Proceso[] salida = new Proceso[2];
						salida[0] = ejecucion;
						salida[1] = null;
						salida[0].setT_final(tiempoActual);
						this.vaciarSC();
						return salida;
					}
				break;
				case "ROUNDROBBIN":
					//Se ejecuta tan solo que el quantum permita
					if (duracionProceso < ejecucion.getT_rafaga() && duracionProceso<tiempoMaximoEjecucion) {
						ejecucion.setT_final(tiempoActual);
						Proceso[] salida = {null,null};
						return salida;

					//Si se pudo ejecutar todo
					} else if (duracionProceso == ejecucion.getT_rafaga()) {
						Proceso[] salida = new Proceso[2];
						salida[0] = ejecucion;
						salida[1] = null;
						duracionProceso = tiempoActual -1;
						salida[0].setT_final(tiempoActual);
						this.vaciarSC();
						return salida;
					//Si se llego al tope del quantum
					} else if (duracionProceso == tiempoMaximoEjecucion) {
						Proceso[] salida = this.partirProceso(duracionProceso, ejecucion);
						this.vaciarSC();
						return salida;
					}

				break;
				case "PRIORIDAD":
					//Se ejecuta todo el proceso
					if (duracionProceso < ejecucion.getT_rafaga()) {
						ejecucion.setT_final(tiempoActual);
						Proceso[] salida = {null,null};
						return salida;
					} else if (duracionProceso == ejecucion.getT_rafaga()) {
						Proceso[] salida = new Proceso[2];
						salida[0] = ejecucion;
						salida[1] = null;
						salida[0].setT_final(tiempoActual);
						this.vaciarSC();
						return salida;
					}
					break;
				}
				
			} else {
				Proceso actual = ejecucion;
				int duracionProc = duracionProceso;
				this.vaciarSC();
				bloquearSiguiente = false;
				return partirProcesoBloqueado(duracionProc, actual);
			}
		}else {
			System.out.println("No hay ningun proceso para seguir avanzando");
			
			Proceso[] salida = {null,null};
			return salida;
		}
		return null;
	}

	public Proceso[] partirProceso(int dondePartir, Proceso proceso) {
		Proceso proc1 = proceso;
		Proceso proc2 = new Proceso(proceso.getNombre(), proceso.getT_rafaga(), proceso.getPrioridadGeneral());

		int duracion1 = dondePartir;
		int duracion2 = proceso.getT_rafaga() - dondePartir;

		// Se cambia la rafaga
		proc1.setT_rafaga(duracion1);
		proc1.setT_final(tiempoActual);

		
		proc2.setT_rafaga(duracion2);
		proc2.setT_comienzo(proceso.getT_comienzo());
		proc2.setT_llegada(proceso.getT_llegada());
		Proceso[] salida = { proc1, proc2 };

		return salida;
	}
	public Proceso[] partirProcesoBloqueado(int dondePartir, Proceso proceso) {
		Proceso proc1 = proceso;
		Proceso proc2 = new Proceso(proceso.getNombre(), proceso.getT_rafaga(), proceso.getPrioridadGeneral());

		int duracion1 = dondePartir;
		int duracion2 = proceso.getT_rafaga() - dondePartir;

		// Se cambia la rafaga
		proc1.setT_rafaga(duracion1);
		proc1.setT_final(tiempoActual);
		// Se establece que lo que falta debe ir a bloqueados
		proc2.setT_rafaga(duracion2);
		proc2.setT_comienzo(proceso.getT_comienzo());
		proc2.setT_llegada(proceso.getT_llegada());
		proc2.setBloqueado(true);

		Proceso[] salida = { proc1, proc2 };

		return salida;
	}

	public Proceso getProcesoActual() {
		return ejecucion;
	}

	public void vaciarSC() {
		ejecucion = null;
		duracionProceso = 0;
	}

	public void bloquearSiguiente() {
		bloquearSiguiente = true;
	}
	public int getTiempoActual() {
		return tiempoActual;
	}

}

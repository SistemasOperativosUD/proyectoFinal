package logica;

import logica.colas.InfoCola;

public class Proceso {
	String nombre;
	String estado;
	int t_llegada;
	int t_rafaga;
	int t_comienzo;
	int t_final;
	int t_retorno;
	int t_espera;
	int prioridadGeneral;
	boolean bloqueado = false;
	//Usado para que la seccion critica pueda saber si es expulsivo o no
	InfoCola informacion = null;
	//Usado para la cola de prioridad
	int prioridadInterna;


	/**
	 * Contructor de un proceso
	 * @param nombre
	 * @param tiempo de rafaga
	 */
	public Proceso (String nombre, int t_rafaga, int prioridadGeneral) {
		this.nombre = nombre;
		this.estado = "SIN ESTADO";
		this.t_rafaga = t_rafaga;
		this.prioridadGeneral = prioridadGeneral;
		
		this.t_llegada = -1;
		this.t_comienzo = -1;
		this.t_final = -1;
		this.t_retorno = -1;
		this.t_espera = -1;
	}
	
	/**
	 * Contructor de un proceso
	 * @param nombre
	 * @param tiempo de rafaga
	 */
	public Proceso (String nombre, int t_rafaga, int t_llegada, int prioridadGeneral) {
		this.nombre = nombre;
		this.estado = "ESPERA";
		this.t_rafaga = t_rafaga;
		this.prioridadGeneral = prioridadGeneral;
		this.t_llegada = t_llegada;
		
		this.t_comienzo = -1;
		this.t_final = -1;
		this.t_retorno = -1;
		this.t_espera = -1;
	}
	/*************************************************/
	/***********GETTERS y SETTERS ********************/
	/************************************************/

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
		this.refrescarCalculos();
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
		this.refrescarCalculos();
	}

	public int getT_llegada() {
		return t_llegada;
	}

	public void setT_llegada(int t_llegada) {
		this.t_llegada = t_llegada;
		this.refrescarCalculos();
	}

	public int getT_rafaga() {
		return t_rafaga;
	}

	public void setT_rafaga(int t_rafaga) {
		this.t_rafaga = t_rafaga;
		this.refrescarCalculos();
	}

	public int getT_comienzo() {
		return t_comienzo;
	}

	public void setT_comienzo(int t_comienzo) {
		this.t_comienzo = t_comienzo;
		this.refrescarCalculos();
	}

	public int getT_final() {
		return t_final;
	}

	public void setT_final(int t_final) {
		this.t_final = t_final;
		this.refrescarCalculos();
	}

	public int getT_retorno() {
		return t_retorno;
	}

	public void setT_retorno(int t_retorno) {
		this.t_retorno = t_retorno;
		this.refrescarCalculos();
	}

	public int getT_espera() {
		return t_espera;
	}

	public void setT_espera(int t_espera) {
		this.t_espera = t_espera;
		this.refrescarCalculos();
	}

	public int getPrioridadGeneral() {
		return prioridadGeneral;
	}

	public void setPrioridadGeneral(int prioridadGeneral) {
		this.prioridadGeneral = prioridadGeneral;
		this.refrescarCalculos();
	}
	
	public boolean getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean estado) {
		bloqueado = estado;
		this.refrescarCalculos();
	}

	public void setInfoCola(InfoCola infoCola) {
		this.informacion = infoCola;
		this.refrescarCalculos();
		
	}
	public InfoCola getInfoCola() {
		return this.informacion;
	}
	
	public void refrescarCalculos() {
		//Se calcula el tiempo de espera
		if(this.t_llegada!= -1 && this.t_comienzo != -1) {
			this.t_espera = t_comienzo - t_llegada;
		}
		if(this.t_llegada != -1 && this.t_final != -1) {
			this.t_retorno = this.t_final-this.t_llegada;
		}
	}
	public int getPrioridadInterna() {
		return prioridadInterna;
	}
	public void setPrioridadInterna(int prioridadInterna) {
		this.prioridadInterna=prioridadInterna;
	}
	
	
}

package logica.colas;

import java.util.ArrayList;

import logica.Proceso;

public class RoundRobbin extends Cola {

	ArrayList<Proceso> cola = new ArrayList<Proceso>();
	final int QUANTUM = 4;

	@Override
	public void agregar(Proceso proceso) {
		// Se le pone al proceso el estado "ESPERA"
		this.setEstadoEspera(proceso);
		proceso.setInfoCola(new InfoCola("ROUNDROBBIN", 3));
		// Se agrega el proceso
		cola.add(proceso);
	}

	@Override
	public Proceso desencolar() {
		if (tieneProceso()) {
			Proceso salida = cola.get(0);
			cola.remove(0);
			return salida;
		} else {
			return null;
		}
	}

	@Override
	public boolean tieneProceso() {
		if (cola.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Proceso[] getProcesos() {
		Proceso[] salida = new Proceso[cola.size()];
		for (int i = 0; i < cola.size(); i++) {
			salida[i] = cola.get(i);
		}
		// TODO Auto-generated method stub
		return salida;
	}

	@Override
	public int getTamano() {
		return cola.size();
	}
	public Proceso desencolar(Proceso proceso) {
		for(int i = 0 ; i<cola.size() ; i++) {
			if(cola.get(i) == proceso) {
				Proceso retornar = cola.get(i);
				cola.remove(i);
				return retornar;
			}
		}
		return null;
	}

}

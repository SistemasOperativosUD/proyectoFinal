package logica.colas;

import java.util.ArrayList;

import logica.Proceso;

public class RafagaCorta extends Cola{
	
	ArrayList<Proceso> cola = new ArrayList<Proceso>();
	@Override
	public void agregar(Proceso proceso) {
		//Se le coloca al proceso el estado "ESPERA"
		this.setEstadoEspera(proceso);
		proceso.setInfoCola(new InfoCola("SFJ"));
		//Se agrega el proceso 
		cola.add(proceso);
		
	}

	@Override
	public Proceso desencolar() {
		int mejorRafaga = 9999;
		int mejorPosicion = 0;
		for(int i = 0 ; i<cola.size();i++) {
			if(cola.get(i).getT_rafaga() < mejorRafaga) {
				mejorRafaga = cola.get(i).getT_rafaga();
				mejorPosicion=i;
			}
		}
		Proceso salida = cola.get(mejorPosicion);
		cola.remove(mejorPosicion);
		return salida;
	}

	@Override
	public boolean tieneProceso() {
		if (cola.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Proceso[] getProcesos() {
		Proceso[] salida = new Proceso[cola.size()];
		for(int i = 0 ; i<cola.size();i++) {
			salida[i]=cola.get(i);
		}
		// TODO Auto-generated method stub
		return salida;
	}

	@Override
	public int getTamano() {
		return cola.size();
	}
	public Proceso desencolar(Proceso proceso) {
		for(int i = 0 ; i<cola.size() ; i++) {
			if(cola.get(i) == proceso) {
				Proceso retornar = cola.get(i);
				cola.remove(i);
				return retornar;
			}
		}
		return null;
	}

}

package logica.colas;

public class InfoCola {
	//Usado para todos
	String algoritmo;
	
	//Usado para round robbin
	int quantum;
	
	
	public InfoCola(String algoritmo, int quantum) {
		this.algoritmo=algoritmo;
		this.quantum = quantum;
	}
	public InfoCola(String algoritmo) {
		this.algoritmo=algoritmo;
		this.quantum = -1;
	}
	public String getAlgoritmo() {
		return this.algoritmo;
	}
	public int getQuantum() {
		return this.quantum;
	}
}

package logica.colas;

import java.util.ArrayList;

import logica.Proceso;

public class Prioridad extends Cola {
	ArrayList<Proceso> cola = new ArrayList<Proceso>();

	@Override
	public void agregar(Proceso proceso) {
		// Se le coloca al proceso el estado "ESPERA"
		this.setEstadoEspera(proceso);
		proceso.setInfoCola(new InfoCola("PRIORIDAD"));
		// Se agrega el proceso
		cola.add(proceso);
	}

	@Override
	public Proceso desencolar() {
		//Se maneja una prioridad de 0 a 5
		int mejorPrioridad = 5;
		int mejorPosicion = 0;
		for(int i = 0 ; i<cola.size();i++) {
			if(cola.get(i).getPrioridadInterna() < mejorPrioridad) {
				mejorPrioridad = cola.get(i).getPrioridadInterna();
				mejorPosicion=i;
			}
		}
		Proceso salida = cola.get(mejorPosicion);
		cola.remove(mejorPosicion);
		return salida;

	}

	@Override
	public boolean tieneProceso() {
		if (cola.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Proceso[] getProcesos() {
		Proceso[] salida = new Proceso[cola.size()];
		for(int i = 0 ; i<cola.size();i++) {
			salida[i]=cola.get(i);
		}
		return salida;
	}

	@Override
	public int getTamano() {
		return cola.size();
	}

	@Override
	public Proceso desencolar(Proceso proceso) {
		for(int i = 0 ; i<cola.size() ; i++) {
			if(cola.get(i) == proceso) {
				Proceso retornar = cola.get(i);
				cola.remove(i);
				return retornar;
			}
		}
		return null;
	}

}

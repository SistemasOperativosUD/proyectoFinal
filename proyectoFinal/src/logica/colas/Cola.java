package logica.colas;

import logica.Proceso;

public abstract class Cola {
	public abstract void agregar(Proceso proceso);
	public abstract Proceso desencolar();
	public abstract Proceso desencolar(Proceso proceso);
	//Solo sus hijos podran usarlo
	protected void setEstadoEspera(Proceso proceso) {
		proceso.setEstado("ESPERA");
	}
	public abstract boolean tieneProceso();
	public abstract Proceso[] getProcesos();
	public abstract int getTamano();
}
